<?PHP

/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Sep 30, 2019 - 3:28:44 PM
 * Filename     : core_helper.php
 * Encoding     : UTF-8
 */

$CI = get_instance();

function konter($param) {
    return $GLOBALS['CI']->Db->query("SELECT news_id FROM pengunjung WHERE news_id = " . $param . " GROUP BY news_id")->row_array();
}

function agentIsExist($param) {
    $broser = get_browser(null, true);
    $exist = $GLOBALS['CI']->Db->query("SELECT news_id FROM pengunjung WHERE news_id = " . $param . " AND os = '" . $broser['platform'] . "' AND peramban = '" . $broser['parent'] . "' AND ip = '" . getClientIP() . "' GROUP BY news_id")->row_array();
}

function alamatIP() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } else if (isset($_SERVER['REMOTE_ADDR'])) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }

    $get = json_decode(file_get_contents("http://ipinfo.io/$ipaddress/geo"), TRUE);
    $alamat = $get['city'] . " - " . $get['region'] . "/" . $get['country'];

    return $alamat;
}

function getClientIP() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } else if (isset($_SERVER['REMOTE_ADDR'])) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }

    return $ipaddress;
}

function kon($param) {
    $resArr = $GLOBALS['CI']->Db->get('konfig')->result_array();

    return $resArr[0][$param];
}

function p_code($param) {
    echo "<pre>";
    print_r($param);
    echo "</pre>";
}

function getMaxBerita($param) {
    $res = $GLOBALS['CI']->Db->get('positioning', array('id_position' => $param))->row_array();

    return $res['max_content'];
}

function convertUsername($param) {
    $res = $GLOBALS['CI']->Db->get('pengguna', array('id' => $param))->row_array();

    return strtolower($res['panggilan']);
}

function convertPosition($param) {
    $res = $GLOBALS['CI']->Db->get('positioning', array('id_position' => $param))->row_array();

    $s1 = trim($GLOBALS['CI']->uri->segment(1));
    $s2 = trim($GLOBALS['CI']->uri->segment(2));

    if ($s1) {
        return strtolower($res['position_title']);
    } else {
        return strtolower($res['position_title']);
    }

    return strtolower($res['position_title']);
}

function bu() {
    return base_url();
}

function urlen($param) {
    return strtolower(str_replace(array(" ", ",", "?"), "-", $param));
}

function get($table, $where = NULL, $limit = NULL, $orderby = NULL) {
    if ($where)
        $GLOBALS['CI']->db->where($where);
    if ($limit)
        $GLOBALS['CI']->db->limit($limit);
    if ($orderby)
        $GLOBALS['CI']->db->order_by($orderby);

    $res = $GLOBALS['CI']->Db->get($table);

//    p_code($GLOBALS['CI']->db->last_query());

    return $res;
}

function get_or_where($table, $where = NULL, $orWhere = NULL, $limit = NULL, $orderby = NULL) {
    if ($where)
        $GLOBALS['CI']->db->where($where);
    if ($orWhere)
        $GLOBALS['CI']->db->or_where($orWhere);
    if ($limit)
        $GLOBALS['CI']->db->limit($limit);
    if ($orderby)
        $GLOBALS['CI']->db->order_by($orderby);

    $res = $GLOBALS['CI']->Db->get($table);

//    p_code($GLOBALS['CI']->db->last_query());

    return $res;
}

function orCariTag($table, $where = NULL, $limit) {
    if ($where) {
        foreach ($where AS $row) {
            $GLOBALS['CI']->db->or_like(array('keywords' => $row));
        }
    }

    $GLOBALS['CI']->db->where(array('tayang' => 1));

    if ($limit)
        $GLOBALS['CI']->db->limit($limit);

    $res = $GLOBALS['CI']->db->get($table);

    return $res;
}

function cari($table, $where = NULL) {
    if ($where)
        $GLOBALS['CI']->db->like($where);
    $GLOBALS['CI']->db->where(array('tayang' => 1));

    $res = $GLOBALS['CI']->db->get($table);

    return $res;
}

function getKanalName($param) {
    $res = $GLOBALS['CI']->Db->get('kanal', array('id_kanal' => $param))->row_array();
    return $res['nama_kanal'];
}

function getKanalSlug($param) {
    $res = $GLOBALS['CI']->Db->get('kanal', array('id_kanal' => $param))->row_array();
    return $res['slug'];
}

function getAuthorName($param) {
    $res = $GLOBALS['CI']->Db->get('pengguna', array('id' => $param))->row_array();
    return $res['nama'];
}

function getReadableDateTime($param) {
//    $ret = explode(" ", $param);
//    $date = explode("-", $ret[0]);
//    $time = $ret[1];
//    $ambilBulan = DateTime::createFromFormat('!m', $date[1]);
//    $namaBulan = $ambilBulan->format('F');
//
//    $jam = date('h:i A', strtotime($time));
//
//    $date = $date[2] . " " . $namaBulan . " " . $date[0] . " " . $jam;
//    return $date;

    return date("d.m.Y H:i", $param);
}

function toAgo($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime("@" . $datetime);

    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'tahun',
        'm' => 'bulan',
        'w' => 'minggu',
        'd' => 'hari',
        'h' => 'jam',
        'i' => 'minit',
        's' => 'detik',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' yang lalu' : 'saat ini';
}

function batasiKonten($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

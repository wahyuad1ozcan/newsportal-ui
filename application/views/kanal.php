<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : resonansi.com
 * Generated    : Oct 2, 2019 - 6:26:22 PM
 * Filename     : kanal.php
 * Encoding     : UTF-8
 */
$kanal = $this->uri->segment(1);
$sKanal = $this->uri->segment(2);

if ($kanal == 'cari') {
    $kanalName = $kanal . ": " . $this->input->get('q');
} else {
    $kanalName = $kanal;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?PHP $this->load->view('inc/head') ?>
    </head>
    <body>
        <!--<div class="se-pre-con"></div>-->
        <?PHP $this->load->view('inc/header') ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <article class="content">
                        <div class="breadcrumbs" style="margin-bottom: 20px;">
                            <ul>
                                <li><i class="pe-7s-home"></i> <a href="<?= bu() ?>" title="">BERANDA</a></li>
                                <li><a title=""><?= strtoupper($kanalName) ?></a></li>
                                <?PHP
                                if ($sKanal) {
                                    if ($kanal == "tag") {
                                        echo '<li><a title="">' . strtoupper(str_replace("-", " ", $sKanal)) . '</a></li>';
                                    } else {
                                        $kanalID = get('kanal', array('slug' => $sKanal))->row_array();
                                        ?>
                                        <li><a title=""><?= strtoupper($kanalID['nama_kanal']) ?></a></li>
                                        <?PHP
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="articale-list">
                            <div class="headding-border"></div>
                            <?PHP foreach ($data AS $row) { ?>
                                <div class="post-style2 wow fadeIn" data-wow-duration="1s" style="border-bottom: 1px solid #ccc;padding-bottom: 20px;">
                                    <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                                        <?PHP
                                        if (strpos($row->media, 'mp4') !== false) {
                                            ?>
                                            <video width="300" controls>
                                                <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                                Your browser does not support HTML5 video.
                                            </video>
                                            <?PHP
                                        } else {
                                            echo "<img alt='" . $row->title . "' class='width-200' src='" . BE_BERITA_DIR . $row->media . "'>";
                                        }
                                        ?>
                                    </a>
                                    <div class="post-style2-detail">
                                        <div style="">
                                            <div style="float: left;"><p style="font-weight: bold;text-transform: uppercase;"><a href="<?= bu() . urlen(getKanalSlug($row->kanal_id)) ?>"><?= getKanalName($row->kanal_id) ?></a></p></div>
                                            <div style="float: right;">
                                                <div class="post-date">
                                                    <i class="pe-7s-clock"></i> <?= toAgo($row->date_published) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both;"></div>
                                        <h4><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" title=""><?= $row->title ?></a></h4>
                                        <p><?= batasiKonten(strip_tags($row->content), 25) ?>...</p>  
                                    </div>
                                </div>
                            <?PHP } ?>
                        </div>
                    </article>
                </div>
                <div class="col-sm-4 left-padding">
                    <aside class="sidebar">
                        <?PHP
                        $this->load->view('mod/resonan');
                        $this->load->view('mod/paling_populer');
                        $this->load->view('mod/video_pilihan');
                        ?>
                    </aside>
                </div>
            </div>
        </div>
        <?PHP $this->load->view("inc/footer") ?>
    </body>
</html>
<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : resonansi.com
 * Generated    : Oct 1, 2019 - 1:54:03 AM
 * Filename     : head.php
 * Encoding     : UTF-8
 */
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?= bu() ?>favicon.png" />

<?PHP if ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'index.php') { ?>
    <title><?= kon('nama_situs') ?> :: <?= kon('desc_situs') ?></title>
    <meta property="og:title" content="<?= kon('nama_situs') ?> :: <?= kon('desc_situs') ?>" />
    <meta property="og:description" content="<?= kon('glob_desc') ?>" />
    <meta name="keywords" content="<?= kon('glob_key') ?>" />
    <meta name="description" content="<?= kon('glob_desc') ?>" />
<?PHP } else { ?>
    <title><?= ($this->uri->segment(1) == 'pratayang') ? "Pratayang Berita: " : "" ?><?= $data['title'] ?></title>
    <meta property="og:title" content="<?= $data['title'] ?>" />
    <meta property="og:description" content="<?= $data['meta_description'] ?>" />
    <meta name="keywords" content="<?= json_decode($data['keywords']) ?>" />
    <meta name="description" content="<?= $data['meta_description'] ?>" />
<?PHP } ?>
<link rel="canonical" href="<?= bu() ?>" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?= bu() ?>" />
<meta property="og:site_name" content="<?= kon('nama_situs') ?>" />

<link rel="stylesheet" href="<?= bu() ?>static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/owl-carousel/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/owl-carousel/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/owl-carousel/owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/css/RYPP.css" />
<link rel="stylesheet" href="<?= bu() ?>static/css/jquery-ui.css">
<link rel="stylesheet" href="<?= bu() ?>static/css/animate.min.css">
<link rel="stylesheet" href="<?= bu() ?>static/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/css/Pe-icon-7-stroke.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/css/flaticon.css" />
<link rel="stylesheet" href="<?= bu() ?>static/css/resonansi.com.css?v=<?= rand(0, 999) ?>">
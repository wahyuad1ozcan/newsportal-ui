<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Oct 1, 2019 - 1:52:45 AM
 * Filename     : footer.php
 * Encoding     : UTF-8
 */
?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="footer-box footer-logo-address">
                    <!-- address  -->
                    <a href="<?= bu() ?>"><img src="<?= bu() ?>static/images/logo.png" style="max-width: 284px;" class="img-responsive" alt="RESONANSI"></a>
                    <address>
                        Institut Harkat Negeri<br>
                        Jl. Tirtayasa IV, RT 04, RW 02, Kebayoran Baru, Jakarta Selatan, 12160
                        <br> Phone: 0813-1601-1792
                        <br> Email: pers@resonansi.com
                    </address>
                </div>
                <!-- /.address  -->
            </div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-6">
                        <h3 class="category-headding">KANAL</h3>
                        <div class="headding-border bg-color-4"></div>
                        <div class="col-sm-6">
                            <div class="footer-box">
                                <ul style="height: 200px;
                                    display: flex;
                                    flex-direction: column;
                                    flex-wrap: wrap;
                                    width: 340px;">
                                    <?PHP
                                    foreach (get('kanal', array('parent' => 0, 'is_aktif' => TRUE), NULL, 'column_order ASC')->result() AS $row) {
                                        ?>
                                        <li><i class="fa fa-dot-circle-o"></i><a href="<?= bu() . $row->slug ?>"><?= $row->nama_kanal ?></a></li>
                                        <?PHP
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="footer-box">
                            <h3 class="category-headding">Menu</h3>
                            <div class="headding-border bg-color-5"></div>
                            <ul>
                                <li><i class="fa fa-dot-circle-o"></i><a href="<?= bu() ?>tentang">Tentang Kami</a></li>
                                <li><i class="fa fa-dot-circle-o"></i><a href="<?= bu() ?>kp">Kebijakan & Privasi</a></li>
                                <li><i class="fa fa-dot-circle-o"></i><a href="<?= bu() ?>sp">Syarat & Penggunaan</a></li>
                                <li><i class="fa fa-dot-circle-o"></i><a href="<?= bu() ?>sitemap">Sitemap</a></li>
                                <li><i class="fa fa-dot-circle-o"></i><a href="<?= bu() ?>rss">RSS</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>
<div class="sub-footer">
    <!-- sub footer -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p><a href="<?= bu() ?>" class="color-1">RESONANSI</a> &copy; <?= date("Y") ?> &mdash; Hak cipta dilindungi oleh undang-undang. :: <i>Halaman dimuat {elapsed_time} detik.</i></p>
                <div class="social">
                    <ul>
                        <li><a href="#" class="facebook"><i class="fa  fa-facebook"></i> </a></li>
                        <li><a href="#" class="twitter"><i class="fa  fa-twitter"></i></a></li>
                        <li><a href="#" class="youtube"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.sub footer -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="<?= bu() ?>static/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="<?= bu() ?>static/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script type="text/javascript" src="<?= bu() ?>static/js/metisMenu.min.js"></script>
<!-- Scrollbar js -->
<script type="text/javascript" src="<?= bu() ?>static/js/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- animate js -->
<script type="text/javascript" src="<?= bu() ?>static/js/wow.min.js"></script>
<!-- Newstricker js -->
<script type="text/javascript" src="<?= bu() ?>static/js/jquery.newsTicker.js"></script>
<!--  classify JavaScript -->
<script type="text/javascript" src="<?= bu() ?>static/js/classie.js"></script>
<!-- owl carousel js -->
<script type="text/javascript" src="<?= bu() ?>static/owl-carousel/owl.carousel.js"></script>
<!-- youtube js -->
<script type="text/javascript" src="<?= bu() ?>static/js/RYPP.js"></script>
<!-- jquery ui js -->
<script type="text/javascript" src="<?= bu() ?>static/js/jquery-ui.js"></script>
<!-- form -->
<script type="text/javascript" src="<?= bu() ?>static/js/form-classie.js"></script>
<!-- custom js -->
<script type="text/javascript" src="<?= bu() ?>static/js/custom.js"></script>
<script>
    window.onscroll = function () {
        myFunction()
    };

    var header = document.getElementById("header-fixed");

    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("diem-hdr");
        } else {
            header.classList.remove("diem-hdr");
        }
    }
</script>
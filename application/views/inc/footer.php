<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : resonansi.com
 * Generated    : Oct 1, 2019 - 1:52:45 AM
 * Filename     : footer.php
 * Encoding     : UTF-8
 */
?>
<div class="sub-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p><a href="<?= bu() ?>" class="color-0"><?= kon('nama_situs') ?></a> &copy; <?= date("Y") ?> &mdash; Hak cipta dilindungi oleh undang-undang. :: <i>Halaman dimuat {elapsed_time} detik.</i></p>
                <div class="social">
                    <ul>
                        <li><a target="_blank" href="<?= kon('fb') ?>" class="facebook"><i class="fa  fa-facebook"></i> </a></li>
                        <li><a target="_blank" href="<?= kon('tw') ?>" class="twitter"><i class="fa  fa-twitter"></i></a></li>
                        <li><a target="_blank" href="<?= kon('ig') ?>" class="youtube"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= bu() ?>static/js/jquery.min.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/metisMenu.min.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/wow.min.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/jquery.newsTicker.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/classie.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/RYPP.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/form-classie.js"></script>
<script type="text/javascript" src="<?= bu() ?>static/js/custom.js"></script>
<script>
    window.onscroll = function () {
        myFunction()
    };

    var header = document.getElementById("header-fixed");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("diem-hdr");
        } else {
            header.classList.remove("diem-hdr");
        }
    }
</script>
<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Oct 1, 2019 - 1:54:03 AM
 * Filename     : head.php
 * Encoding     : UTF-8
 */
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?= bu() ?>favicon.png" />
<title><?= kon('nama_situs') ?> :: <?= kon('desc_situs') ?></title>

<meta name="keywords" content="<?= kon('glob_key') ?>" />
<meta name="description" content="<?= kon('glob_desc') ?>" />
<link rel="canonical" href="<?= bu() ?>" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?= kon('nama_situs') ?> :: <?= kon('desc_situs') ?>" />
<meta property="og:description" content="<?= kon('glob_desc') ?>" />
<meta property="og:url" content="<?= bu() ?>" />
<meta property="og:site_name" content="<?= kon('nama_situs') ?>" />

<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i|Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
<link rel="stylesheet" href="<?= bu() ?>static/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/css/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/owl-carousel/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/owl-carousel/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/owl-carousel/owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/css/RYPP.css" />
<link rel="stylesheet" href="<?= bu() ?>static/css/jquery-ui.css">
<link rel="stylesheet" href="<?= bu() ?>static/css/animate.min.css">
<link rel="stylesheet" href="<?= bu() ?>static/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/css/Pe-icon-7-stroke.css" />
<link rel="stylesheet" type="text/css" href="<?= bu() ?>static/css/flaticon.css" />
<link rel="stylesheet" href="<?= bu() ?>static/css/resonansi.com.css?v=<?= rand(0, 999) ?>">
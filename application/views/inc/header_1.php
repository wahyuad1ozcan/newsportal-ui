<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Oct 1, 2019 - 1:54:29 AM
 * Filename     : header.php
 * Encoding     : UTF-8
 */
?>
<header>
    <div class="mobile-menu-area navbar-fixed-top hidden-sm hidden-md hidden-lg">
        <nav class="mobile-menu" id="mobile-menu">
            <div class="sidebar-nav">
                <ul class="nav side-menu">
                    <li class="sidebar-search">
                        <form action="<?= bu() ?>cari" method="get">
                            <div class="input-group custom-search-form">
                                <input style="color: #fff;" name="q" type="text" class="form-control" placeholder="Cari berita..." value="<?= $this->input->get('q') ?>">
                                <span class="input-group-btn">
                                    <button class="btn mobile-menu-btn" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </li>
                    <li><a href="<?= bu() ?>">BERANDA</a></li>
                    <?PHP
                    foreach (get('kanal', array('ayah' => 0, 'aktif' => TRUE), NULL, 'urutan ASC')->result() AS $row) {
                        ?>
                        <li><a href="<?= bu() . $row->slug ?>"><?= $row->nama_kanal ?></a></li>
                        <?PHP
                    }
                    ?>
                    <li>
                        <div class="social">
                            <ul>
                                <li><a href="<?= kon('fb') ?>" class="facebook"><i class="fa  fa-facebook"></i> </a></li>
                                <li><a href="<?= kon('tw') ?>" class="twitter"><i class="fa  fa-twitter"></i></a></li>
                                <li><a href="<?= kon('ig') ?>" class="youtube"><i class="fa  fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <div class="top_header_icon">
                <span class="top_header_icon_wrap">
                    <a target="_blank" href="<?= kon('tw') ?>" title="Twitter"><i class="fa fa-twitter"></i></a>
                </span>
                <span class="top_header_icon_wrap">
                    <a target="_blank" href="<?= kon('fb') ?>" title="Facebook"><i class="fa fa-facebook"></i></a>
                </span>
                <span class="top_header_icon_wrap">
                    <a target="_blank" href="<?= kon('ig') ?>" title="Google"><i class="fa fa-youtube"></i></a>
                </span>
            </div>
            <div id="showLeft" class="nav-icon">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <div class="top_banner_wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4 col-sm-4">
                    <div class="header-logo">
                        <!-- logo -->
                        <a href="<?= bu() ?>">
                            <img class="td-retina-data img-responsive" src="<?= BE_ASSETS_DIR ?>gambar/<?= kon('logo') ?>" alt="<?= kon('nama_situs') ?>">
                        </a>
                    </div>
                </div>
                <div class="col-xs-5 col-md-5 col-sm-5 hidden-xs">
                    <form action="<?= bu() ?>cari" method="get">
                        <div class="header-banner">
                            <div class="input-group search-area">
                                <input type="text" class="form-control" placeholder="Masukkan beberapa kata untuk mencari berita..." name="q" value="<?= $this->input->get('q') ?>">
                                <div class="input-group-btn">
                                    <button style="border-radius: 25px;border-top-left-radius: 0;border-bottom-left-radius: 0;background: #F26522;" class="btn btn-search" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-3 col-md-3 col-sm-3 hidden-xs">
                    <a href="<?= BE_URL ?>" style="color: #fff;">
                        <div style="float: right;
                             margin: 29px 0 0 0;
                             font-weight: bold;
                             background: #F26522;
                             padding: 5px 20px;
                             border-radius: 20px;
                             border-top-right-radius: 0;
                             border-bottom-left-radius: 0;">
                            MASUK
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="header-fixed">
        <div class="container hidden-xs">
            <nav class="navbar">
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?= bu() ?>" class="category0 <?= (($this->uri->segment(1) == "") ? "aktif" : "") ?>">BERANDA</a></li>
                        <?PHP
                        $no = 1;
                        foreach (get('kanal', array('ayah' => 0, 'aktif' => TRUE), NULL, 'urutan ASC')->result() AS $row) {
                            $slug[] = $row->slug;
                            ?>
                            <li><a href="<?= bu() . $row->slug ?>" class="category<?= $no ?>  <?= (($this->uri->segment(1) == $row->slug) ? "aktif" : "") ?>"><?= strtoupper($row->nama_kanal) ?></a></li>
                            <?PHP
                            $no++;
                        }
                        ?>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="container hidden-xs">
            <nav class="navbar" style="background: #f8f8ff;">
                <?PHP
                if (in_array($this->uri->segment(1), $slug)) {
                    $kanalID = get('kanal', array('slug' => $this->uri->segment(1)))->row_array();
                    $subKanal = get('kanal', array('ayah' => $kanalID['id_kanal'], 'aktif' => TRUE))->result();
                    ?>
                    <div style="float: left;padding: 12px;color: #000;font-weight: bold;">
                        <div style="float: left;margin-right: 8px;width: 20px;height: 20px;background-image: url('<?= BE_UPLOAD_DIR ?>cat-ico/<?= $kanalID['ikon'] ?>');background-size: 20px;background-repeat: no-repeat;"></div>
                        <?= strtoupper($kanalID['nama_kanal']) ?>:
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav" style="display: inline-block;float: none;">
                            <?PHP foreach ($subKanal AS $row) { ?>
                                <li class="active"><a href="<?= bu() . $this->uri->segment(1) . "/" . $row->slug ?>" class=" active"> <?= $row->nama_kanal ?></a></li>
                            <?PHP } ?>
                        </ul>
                    </div>
                    <?PHP
                } else {
                    ?>
                    <div style="float: left;padding: 12px;color: #fff;font-weight: bold;background: #F26522;">
                        TOPIK HARI INI:
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav" style="display: inline-block;float: none;">
                            <?PHP foreach (get('thi', NULL, 5)->result() AS $row) { ?>
                                <li class="active"><a href="<?= bu() ?>tag/<?= trim($row->nama_thi) ?>" class=" active">#<?= trim($row->nama_thi) ?></a></li>
                                <?PHP } ?>
                        </ul>
                    </div> 
                <?PHP } ?>
            </nav>
        </div>
    </div>
</header>
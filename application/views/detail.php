<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : resonansi.com
 * Generated    : Oct 1, 2019 - 9:56:11 PM
 * Filename     : detail.php
 * Encoding     : UTF-8
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?PHP $this->load->view('inc/head') ?>
    </head>
    <body>
        <!--<div class="se-pre-con"></div>-->
        <?PHP $this->load->view('inc/header') ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <article class="content">
                        <div class="breadcrumbs">
                            <ul>
                                <li><i class="pe-7s-home"></i> <a href="<?= bu() ?>" title="">BERANDA</a></li>
                                <li><a href="<?= bu() . $data['kanal_slug'] ?>" title=""><?= strtoupper($data['kanal_slug']) ?></a></li>
                            </ul>
                        </div>
                        <h1><?= $data['title'] ?></h1>
                        <div class="date">
                            <ul>
                                <li><a title="" href="#!"><span><?= $data['author_name'] ?></span></a> --</li>
                                <li><a><?= $data['readable_publish_date'] ?></a></li>
                            </ul>
                        </div>
                        <div class="shareBtm">
                            <div class="shareButton">
                                <div class="bss" >
                                    <span class='st_sharethis_hcount' displayText='ShareThis'></span>
                                    <span class='st_facebook_hcount' displayText='Facebook'></span>
                                    <span class='st_twitter_hcount' displayText='Tweet'></span>
                                    <script type="text/javascript">var switchTo5x = true;</script>
                                    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
                                    <script type="text/javascript">stLight.options({publisher: "5dc9678d-5925-46e1-8f2c-e74ca68e941d", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
                                </div>
                            </div>
                        </div>
                        <div class="post-thumb" style="margin-top: 20px;">
                            <?PHP
                            if (strpos($data['img_url'], 'mp4') !== false) {
                                ?>
                                <video width="300" controls>
                                    <source src="<?= $data['img_url'] ?>" type="video/mp4">
                                    Your browser does not support HTML5 video.
                                </video>
                                <?PHP
                            } else {
                                echo "<img alt='" . $data['title'] . "' class='img-responsive post-image' src='" . $data['img_url'] . "'>";
                            }
                            ?>
                        </div>
                        <div style="border:1px solid #ccc;background: #F6F6F6;padding: 5px;margin-bottom: 20px;text-align: center;"><?= $data['media_caption'] ?></div>
                        <?= str_replace(array("<pre>", "</pre>"), "", $data['content']) ?>
                        <!-- tags -->
                        <div class="tags">
                            <div style="float: left;
                                 padding: 8px 17px 8px 8px;
                                 background-color: #f60d2b;
                                 color: #fff;
                                 border-radius: 25px;
                                 border-top-left-radius: 0;
                                 border-bottom-left-radius: 0;
                                 margin: -5px 15px 0 0;">Tag Berita:</div> 
                            <ul>
                                <?PHP
                                $tags = json_decode($data['keywords']);
                                $tags = explode(",", $tags);

                                foreach ($tags AS $row) {
                                    $orLikeForBeritaTerkait[] = trim($row);
                                    ?>
                                    <li><a href="<?= bu() ?>tag/<?= trim($row) ?>"><?= $row ?></a></li>
                                    <?PHP
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="related-news-inner">
                            <h3 class="category-headding ">BERITA TERKAIT</h3>
                            <div class="headding-border"></div>
                            <div class="row">
                                <div id="content-slide-5" class="owl-carousel">
                                    <div class="item">
                                        <div class="row rn_block">
                                            <?PHP foreach (orCariTag('news', $orLikeForBeritaTerkait, 3)->result() AS $row) { ?>
                                                <div class="col-xs-12 col-md-4 col-sm-4 padd">
                                                    <div class="post-wrapper wow fadeIn" data-wow-duration="2s">
                                                        <div class="post-thumb">
                                                            <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                                                                <?PHP
                                                                if (strpos($row->media, 'mp4') !== false) {
                                                                    ?>
                                                                    <video width="300" controls>
                                                                        <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                                                        Your browser does not support HTML5 video.
                                                                    </video>
                                                                    <?PHP
                                                                } else {
                                                                    echo "<img alt='" . $data['title'] . "' class='img-responsive' src='" . BE_BERITA_DIR . $row->media . "'>";
                                                                }
                                                                ?>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="post-title-author-details">
                                                        <h4><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>"><?= $row->title ?></a></h4>
                                                        <div class="post-editor-date">
                                                            <div class="post-date">
                                                                <i class="pe-7s-clock"></i> <?= toAgo($row->date_published) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?PHP } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- form
                            ============================================ -->
                        <div class="form-area">
                            <h3 class="category-headding ">TINGGALKAN KOMENTAR</h3>
                            <div class="headding-border"></div>
                            <div class="fb-comments" data-href="<?= current_url() ?>" data-width="100%" data-numposts="5"></div>
<!--                            <div id="disqus_thread"></div>
                            <script>

                                /**
                                 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                /*
                                 var disqus_config = function () {
                                 this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                                 this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                 };
                                 */
                                (function () { // DON'T EDIT BELOW THIS LINE
                                    var d = document, s = d.createElement('script');
                                    s.src = 'https://eviralo.disqus.com/embed.js';
                                    s.setAttribute('data-timestamp', +new Date());
                                    (d.head || d.body).appendChild(s);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>-->
                        </div>
                    </article>
                </div>
                <div class="col-sm-4 left-padding">
                    <aside class="sidebar">
                        <?PHP
                        $this->load->view('mod/resonan');
                        $this->load->view('mod/paling_populer');
                        $this->load->view('mod/video_pilihan');
                        ?>
                    </aside>
                </div>
            </div>
        </div>
        <?PHP $this->load->view("inc/footer") ?>
    </body>
</html>
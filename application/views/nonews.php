<?PHP

/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : resonansi.com
 * Generated    : Oct 23, 2019 - 5:05:18 PM
 * Filename     : nonews.php
 * Encoding     : UTF-8
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?PHP $this->load->view('inc/head') ?>
        <link rel="stylesheet" type="text/css" href="<?= bu() ?>static/plugins/ganti-slider/slick.css"/>
        <link rel="stylesheet" type="text/css" href="<?= bu() ?>static/plugins/ganti-slider/slick-theme.css"/>
    </head>
    <body>
        <!--<div class="se-pre-con"></div>-->
        <?PHP $this->load->view('inc/header') ?>
        <section class="headding-news">
            <div class="container">
                <div class="row row-margin">
                    <div class="col-sm-12 col-padding" style="margin-bottom: 20px;border-bottom: 1px solid #ccc;">
                        <div class="row-margin-bottom gak-ketemu">
                            <div style="font-size: 110px;">0.0</div>
                            <div style="font-size: 30px;">Tidak ada berita di Kanal ini</div>
                            <div style="font-style: italic;padding: 25px;">Nampaknya anda mengakses kanal yang tidak memiliki berita</div>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="col-sm-4 row-margin-top">
                        <?PHP
                        $this->load->view('mod/resonan');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?PHP
                        $this->load->view('mod/paling_populer');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?PHP
                        $this->load->view('mod/video_pilihan');
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <?PHP $this->load->view("inc/footer") ?>
    </body>
</html>
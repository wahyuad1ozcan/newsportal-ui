<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : resonansi.com
 * Generated    : Sep 30, 2019 - 3:27:27 PM
 * Filename     : index.php
 * Encoding     : UTF-8
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?PHP $this->load->view('inc/head') ?>
        <link rel="stylesheet" type="text/css" href="<?= bu() ?>static/plugins/ganti-slider/slick.css"/>
        <link rel="stylesheet" type="text/css" href="<?= bu() ?>static/plugins/ganti-slider/slick-theme.css"/>
    </head>
    <body>
        <!--<div class="se-pre-con"></div>-->
        <?PHP $this->load->view('inc/header') ?>
        <section class="headding-news">
            <div class="container">
                <div class="row row-margin">
                    <div class="col-sm-8 col-padding" style="margin-bottom: 20px;">
                        <div class="row-margin-bottom">
                            <div class="slider slider-for">
                                <?PHP
                                $placingID = array('untuk' => 2, 'aktif' => 1);
                                if ($this->uri->segment(1) == '') {
                                    $apaKanal = array('is_kanal IS NULL' => NULL);
                                } else {
                                    if ($this->uri->segment(2) != '')
                                        $apaKanal = array('is_kanal' => 0);
                                    else
                                        $apaKanal = array('is_kanal' => 1);
                                }
                                $kanalID = $isKanal;
                                $where = array_merge($placingID, $kanalID, $apaKanal);
                                foreach (get('frontpage', $where, getMaxBerita(2), 'placing_order ASC')->result() AS $fp) {
                                    $row = get('news', array('id' => $fp->news_id))->row();
                                    ?>
                                    <div class="post-wrapper post-grid-3 wow fadeIn" data-wow-duration="2s">
                                        <div class="post-thumb img-zoom-in">
                                            <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                                                <?PHP
                                                if (strpos($row->media, 'mp4') !== false) {
                                                    ?>
                                                    <video width="300" controls>
                                                        <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                                        Your browser does not support HTML5 video.
                                                    </video>
                                                    <?PHP
                                                } else {
                                                    echo "<img alt='" . $row->title . "' class='entry-thumb-middle' src='" . BE_BERITA_DIR . $row->media . "'>";
                                                }
                                                ?>
                                            </a>
                                        </div>
                                        <div class="post-info">
                                            <span class="color-4"><a style="color: #F36621;" href="<?= bu() . urlen(getKanalSlug($row->kanal_id)) ?>"><?= getKanalName($row->kanal_id) ?></a></span>
                                            <h3 class="post-title"><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" rel="bookmark"><?= $row->title ?></a></h3>
                                        </div>
                                    </div>
                                <?PHP } ?>
                            </div>
                            <div class="slider slider-nav">
                                <?PHP
                                $placingID = array('untuk' => 2, 'aktif' => 1);
                                $kanalID = $isKanal;
                                $where = array_merge($placingID, $kanalID, $apaKanal);
                                foreach (get('frontpage', $where, getMaxBerita(2), 'placing_order ASC')->result() AS $fp) {
                                    $row = get('news', array('id' => $fp->news_id))->row();
                                    ?>
                                    <div class="post-wrapper post-grid-3 wow fadeIn" data-wow-duration="2s">
                                        <div class="post-thumb img-zoom-in">
                                            <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                                                <?PHP
                                                if (strpos($row->media, 'mp4') !== false) {
                                                    ?>
                                                    <video width="300" controls>
                                                        <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                                        Your browser does not support HTML5 video.
                                                    </video>
                                                    <?PHP
                                                } else {
                                                    echo "<img alt='" . $row->title . "' style='max-height:140px;' class='entry-thumb-middle-sub' src='" . BE_BERITA_DIR . $row->media . "'>";
                                                }
                                                ?>
                                            </a>
                                        </div>
                                        <div class="post-info">
                                            <span class="color-4"><a style="color: #F36621;" href="<?= bu() . urlen(getKanalSlug($row->kanal_id)) ?>"><?= getKanalName($row->kanal_id) ?></a></span>
                                            <h5 class="post-title-sub"><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" rel="bookmark"><?= $row->title ?></a></h5>
                                        </div>
                                    </div>
                                <?PHP } ?>
                            </div>
                        </div>
                        <?PHP
                        $placingID = array('untuk' => 4, 'aktif' => 1);
                        $kanalID = $isKanal;
                        $where = array_merge($placingID, $kanalID, $apaKanal);
                        $getBUL = get('frontpage', $where, getMaxBerita(4), 'placing_order ASC')->result();
                        if (count($getBUL) > 0) {
                            ?>
                            <div class="row-margin-top">
                                <h3 class="category-headding "><?= convertPosition(4) //berita utama lainnya                                                                                                      ?></h3>
                                <div class="headding-border"></div>
                                <div class="row">
                                    <div id="berita-utama-lainnya" class="owl-carousel">
                                        <?PHP
                                        foreach ($getBUL AS $fp) {
                                            $row = get('news', array('id' => $fp->news_id))->row();
                                            ?>
                                            <div class="item home2-post col-sm-12 col-md-12">
                                                <div class="post-wrapper wow fadeIn" data-wow-duration="1s">
                                                    <div class="item">
                                                        <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                                                            <?PHP
                                                            if (strpos($row->media, 'mp4') !== false) {
                                                                ?>
                                                                <video width="300" controls>
                                                                    <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                                                    Your browser does not support HTML5 video.
                                                                </video>
                                                                <?PHP
                                                            } else {
                                                                echo "<img alt='" . $row->title . "' class='img-responsive' src='" . BE_BERITA_DIR . $row->media . "'>";
                                                            }
                                                            ?>
                                                        </a>
                                                        <h4><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>"><?= $row->title ?></a></h4>
                                                    </div>
                                                </div>
                                            </div>
                                        <?PHP } ?>
                                    </div>
                                </div>
                            </div>
                            <?PHP
                        }
                        $placingID = array('placing_id' => 5, 'aktif' => 1);
                        $kanalID = $isKanal;
                        $where = array_merge($placingID, $kanalID);
                        $getBHI = get('widget', $where)->result();
                        if (count($getBHI) > 0) {
                            ?>
                            <div class="row-margin-top">
                                <h3 class="category-headding "><?= convertPosition(5) //berita hari ini                                                                                                     ?></h3>
                                <div class="headding-border"></div>
                                <?PHP
                                foreach ($getBHI AS $wid) {
                                    $row = get('news', array('id' => $wid->news_id))->row();
                                    ?>
                                    <div class="post-style2 wow fadeIn" data-wow-duration="1s">
                                        <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                                            <?PHP
                                            if (strpos($row->media, 'mp4') !== false) {
                                                ?>
                                                <video width="300" controls>
                                                    <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                                    Your browser does not support HTML5 video.
                                                </video>
                                                <?PHP
                                            } else {
                                                echo "<img alt='" . $row->title . "' class='width-200' src='" . BE_BERITA_DIR . $row->media . "'>";
                                            }
                                            ?>
                                        </a>
                                        <div class="post-style2-detail">
                                            <div style="">
                                                <div style="float: left;"><p style="font-weight: bold;text-transform: uppercase;"><a style="color: #F36621;" href="<?= bu() . urlen(getKanalSlug($row->kanal_id)) ?>"><?= getKanalName($row->kanal_id) ?></a></p></div>
                                                <div style="float: right;">
                                                    <div class="post-date">
                                                        <i class="pe-7s-clock"></i> <?= toAgo($row->date_published) ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                            <h4><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" title=""><?= $row->title ?></a></h4>
                                            <p><?= batasiKonten(strip_tags($row->content), 25) ?>...</p>  
                                        </div>
                                    </div>
                                <?PHP } ?>
                            </div>
                            <?PHP
                        }
                        $placingID = array('placing_id' => 7, 'aktif' => 1);
                        $kanalID = $isKanal;
                        $where = array_merge($placingID, $kanalID);
                        $getSV = get('widget', $where)->result();
                        if (count($getSV) > 0) {
                            ?>
                            <div class="row-margin-top">
                                <h3 class="category-headding "><?= convertPosition(7) //video                                                                                                ?></h3>
                                <div class="headding-border"></div>
                                <div class="row rn_block">
                                    <?PHP
                                    $placingID = array('placing_id' => 7, 'aktif' => 1);
                                    $kanalID = $isKanal;
                                    $where = array_merge($placingID, $kanalID);
                                    $getSVOne = get('widget', $where, 1)->result();
                                    foreach ($getSVOne AS $wid) {
                                        $row = get('news', array('id' => $wid->news_id))->row();
                                        $getSVIDOne[] = $row->id;
                                        ?>
                                        <div id="widget-res" class="" style="margin-bottom: 20px;">
                                            <div class="col-md-12 col-sm-12 padd">
                                                <div class="item">
                                                    <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                                                        <?PHP
                                                        if (strpos($row->media, 'mp4') !== false) {
                                                            ?>
                                                            <video width="300" controls>
                                                                <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                                                Your browser does not support HTML5 video.
                                                            </video>
                                                            <?PHP
                                                        } else {
                                                            echo "<img alt='" . $row->title . "' class='' src='" . BE_BERITA_DIR . $row->media . "'>";
                                                        }
                                                        ?>
                                                    </a>
                                                    <h4><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>"><?= $row->title ?></a></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <?PHP
                                    }
                                    $placingID = array('placing_id' => 7, 'aktif' => 1);
                                    $kanalID = $isKanal;
                                    $sisaID = array('news_id != ' => $getSVIDOne[0]);
                                    $where = array_merge($placingID, $kanalID, $sisaID);
                                    $getSVOne = get('widget', $where, (getMaxBerita(7) - 1))->result();
                                    foreach ($getSVOne AS $wid) {
                                        $row = get('news', array('id' => $wid->news_id))->row();
                                        ?>
                                        <div class="col-md-4 col-sm-4 padd">
                                            <div class="home2-post">
                                                <div class="post-wrapper wow fadeIn" data-wow-duration="1s">
                                                    <div class="post-thumb">
                                                        <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                                                            <?PHP
                                                            if (strpos($row->media, 'mp4') !== false) {
                                                                ?>
                                                                <video width="300" controls>
                                                                    <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                                                    Your browser does not support HTML5 video.
                                                                </video>
                                                                <?PHP
                                                            } else {
                                                                echo "<img alt='" . $row->title . "' class='img-responsive' src='" . BE_BERITA_DIR . $row->media . "'>";
                                                            }
                                                            ?>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="post-title-author-details">
                                                    <h4><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" title=""><?= $row->title ?></a></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <?PHP
                                    }
                                    ?>
                                </div>
                            </div>
                            <?PHP
                        }
                        $kanalID = array('(placing_id = 0 OR placing_id = 1)' => NULL);
                        $where = array_merge($isKanal, $kanalID);
                        $getSisa = get_or_where('news', $where, NULL, NULL, 'date_approved DESC')->result();
                        if (count($getSisa) > 0) {
                            ?>
                            <div class="row-margin-top">
                                <div class="headding-border"></div>
                                <?PHP
                                foreach ($getSisa AS $row) {
                                    ?>
                                    <div class="post-style2 wow fadeIn" data-wow-duration="1s">
                                        <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                                            <?PHP
                                            if (strpos($row->media, 'mp4') !== false) {
                                                ?>
                                                <video width="300" controls>
                                                    <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                                    Your browser does not support HTML5 video.
                                                </video>
                                                <?PHP
                                            } else {
                                                echo "<img alt='" . $row->title . "' class='width-200' src='" . BE_BERITA_DIR . $row->media . "'>";
                                            }
                                            ?>
                                        </a>
                                        <div class="post-style2-detail">
                                            <div style="">
                                                <div style="float: left;"><p style="font-weight: bold;text-transform: uppercase;"><a style="color: #F36621;" href="<?= bu() . urlen(getKanalSlug($row->kanal_id)) ?>"><?= getKanalName($row->kanal_id) ?></a></p></div>
                                                <div style="float: right;">
                                                    <div class="post-date">
                                                        <i class="pe-7s-clock"></i> <?= toAgo($row->date_published) ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                            <h4><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" title=""><?= $row->title ?></a></h4>
                                            <p><?= batasiKonten(strip_tags($row->content), 25) ?>...</p>  
                                        </div>
                                    </div>
                                <?PHP } ?>
                            </div>
                            <?PHP
                        }
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?PHP
                        $this->load->view('mod/resonan');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?PHP
                        $this->load->view('mod/paling_populer');
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?PHP
                        $this->load->view('mod/video_pilihan');
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <?PHP $this->load->view("inc/footer") ?>
        <script type="text/javascript" src="<?= bu() ?>static/plugins/ganti-slider/slick.min.js"></script>
        <script>
            $(function () {
                $('.slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '.slider-nav'
                });
                $('.slider-nav').slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    dots: true,
                    centerMode: true,
                    focusOnSelect: true,
                    autoplay: true,
                    autoplaySpeed: 5000,
                });
            });
        </script>
    </body>
</html>
<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Oct 22, 2019 - 8:47:32 PM
 * Filename     : paling_populer.php
 * Encoding     : UTF-8
 */

$placingID = array('placing_id' => 6, 'aktif' => 1);
$kanalID = $isKanal;
$where = array_merge($placingID, $kanalID);
$getPP = get('widget', $where)->result();
if (count($getPP) > 0) {
    ?>
    <h3 class="category-headding row-margin-top"><?= convertPosition(6) //paling populer                                                                           ?></h3>
    <div class="headding-border"></div>
    <?PHP
    foreach ($getPP AS $wid) {
        $row = get('news', array('id' => $wid->news_id))->row();
        ?>
        <div class="box-item wow fadeIn" data-wow-duration="1s">
            <div class="img-thumb">
                <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" rel="bookmark">
                    <?PHP
                    if (strpos($row->media, 'mp4') !== false) {
                        ?>
                        <video width="300" controls>
                            <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                            Your browser does not support HTML5 video.
                        </video>
                        <?PHP
                    } else {
                        echo "<img alt='" . $row->title . "' class='entry-thumb' src='" . BE_BERITA_DIR . $row->media . "' height='70' width='80'>";
                    }
                    ?>
                </a>
            </div>
            <div class="item-details-res">
                <h6 class="sub-category-title bg-color-1">
                    <a href="<?= bu() . urlen(getKanalSlug($row->kanal_id)) ?>"><?= getKanalName($row->kanal_id) ?></a>
                </h6>
                <h3 class="td-module-title"><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" title=""><?= $row->title ?></a></h3>
                <div class="post-editor-date">
                    <div class="post-date">
                        <i class="pe-7s-clock"></i> <?= toAgo($row->date_published) ?>
                    </div>
                </div>
            </div>
        </div>
        <?PHP
    }
}
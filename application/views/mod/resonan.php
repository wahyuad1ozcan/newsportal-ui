<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Oct 22, 2019 - 8:47:58 PM
 * Filename     : resonan.php
 * Encoding     : UTF-8
 */

$idGede = array();
$placingID = array('placing_id' => 3, 'aktif' => 1);
$kanalID = $isKanal;
$where = array_merge($placingID, $kanalID);
$getRES = get('widget', $where, 1)->result();
if (count($getRES) > 0) {
    ?>
    <div class="widget-slider-inner">
        <h3 class="category-headding "><?= convertPosition(3) //resonan                                                                              ?></h3>
        <div class="headding-border"></div>
        <div id="widget-res" class="">
            <?PHP
            foreach ($getRES AS $wid) {
                $row = get('news', array('id' => $wid->news_id))->row();
                $idGede[] = $row->id;
                ?>
                <div class="item">
                    <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>">
                        <?PHP
                        if (strpos($row->media, 'mp4') !== false) {
                            ?>
                            <video width="300" controls>
                                <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                                Your browser does not support HTML5 video.
                            </video>
                            <?PHP
                        } else {
                            echo "<img alt='" . $row->title . "' class='' src='" . BE_BERITA_DIR . $row->media . "'>";
                        }
                        ?>
                    </a>
                    <h4><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>"><?= $row->title ?></a></h4>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
    <?PHP
    $placingID = array('placing_id' => 3, 'aktif' => 1);
    $kanalID = $isKanal;
    $sisaID = array('news_id != ' => $idGede[0]);
    $where = array_merge($placingID, $kanalID, $sisaID);

    foreach (get('widget', $where, 2)->result() AS $wid) {
        $row = get('news', array('id' => $wid->news_id))->row();
        ?>
        <div class="box-item wow fadeIn" data-wow-duration="1s">
            <div class="img-thumb">
                <a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" rel="bookmark">
                    <?PHP
                    if (strpos($row->media, 'mp4') !== false) {
                        ?>
                        <video width="300" controls>
                            <source src="<?= BE_BERITA_DIR . $row->media ?>" type="video/mp4">
                            Your browser does not support HTML5 video.
                        </video>
                        <?PHP
                    } else {
                        echo "<img alt='" . $row->title . "' class='entry-thumb' src='" . BE_BERITA_DIR . $row->media . "' height='70' width='80'>";
                    }
                    ?>
                </a>
            </div>
            <div class="item-details-res">
                <h6 class="sub-category-title bg-color-1">
                    <a href="<?= bu() . urlen(getKanalSlug($row->kanal_id)) ?>"><?= getKanalName($row->kanal_id) ?></a>
                </h6>
                <h3 class="td-module-title"><a href="<?= bu() . convertUsername($row->author) . "/" . urlen(getKanalName($row->kanal_id)) . "/" . $row->id . "-" . url_title(strtolower($row->title)) ?>" title=""><?= $row->title ?></a></h3>
            </div>
        </div>
        <?PHP
    }
}
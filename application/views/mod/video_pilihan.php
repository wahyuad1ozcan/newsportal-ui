<?PHP
/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Oct 22, 2019 - 8:48:53 PM
 * Filename     : video_pilihan.php
 * Encoding     : UTF-8
 */
?>
<div class="video-headding row-margin-top">Video Pilihan</div>
<div id="rypp-demo-4" class="RYPP r16-9" data-playlist="PLeDxrQHAn9eZHV79AuqdBPIOYEwDGLiof">
    <div class="RYPP-video">
        <div class="RYPP-video-player">
        </div>
    </div>
    <div class="RYPP-playlist">
        <header>
            <h2 class="_h1 RYPP-title">RESONANSI.com</h2>
            <p class="RYPP-desc">Saluran video akal sehat <a style="font-style: italic;color: #f1f1f1" href="https://www.youtube.com/channel/UCZW_syLBKpYdkktBSGFE1cQ" target="_blank">(Official Channel)</a></p>
        </header>
        <div class="RYPP-items"></div>
    </div>
</div>
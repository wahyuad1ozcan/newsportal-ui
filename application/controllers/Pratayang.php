<?PHP

/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : resonansi.com
 * Generated    : Nov 13, 2019 - 11:25:30 AM
 * Filename     : Pratayang.php
 * Encoding     : UTF-8
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Pratayang extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $kanal = $this->uri->segment(3);
        $id = $this->uri->segment(4);
        $id = explode("-", $id);
        $id = $id[0];

        $kanalSlug = array('kanal_slug' => $kanal);
        $data = get('news_cart', array('id_cart' => $id, 'status' => 0))->row_array();
        $author = array('author_name' => getAuthorName($data['owner']));
        $rpd = array('readable_publish_date' => getReadableDateTime($data['created_date']));
        $ni = array('img_url' => BE_BERITA_DIR . $data['media']);
        $this->data['data'] = array_merge($kanalSlug, $data, $author, $rpd, $ni);
        $this->data['isKanal'] = array();

        if (count($data) == 0) {
            $this->load->view('notfound', $this->data);
        } else {
            $this->load->view('detail', $this->data);
        }
    }

}

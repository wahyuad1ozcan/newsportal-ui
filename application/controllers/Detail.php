<?PHP

/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Oct 1, 2019 - 9:53:56 PM
 * Filename     : Detail.php
 * Encoding     : UTF-8
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->data['broser'] = get_browser(null, true);
    }

    public function index() {
        $kanal = $this->uri->segment(2);
        $id = $this->uri->segment(3);
        $id = explode("-", $id);
        $id = $id[0];

        $kanalSlug = array('kanal_slug' => $kanal);
        $data = get('news', array('id' => $id, 'tayang' => 1))->row_array();
        $author = array('author_name' => getAuthorName($data['author']));
        $rpd = array('readable_publish_date' => getReadableDateTime($data['date_published']));
        $ni = array('img_url' => BE_BERITA_DIR . $data['media']);
        $this->data['data'] = array_merge($kanalSlug, $data, $author, $rpd, $ni);
        $this->data['isKanal'] = array();

        if (count($data) == 0) {
            $this->load->view('notfound', $this->data);
        } else {
            session_start();
            if ($_SESSION['news_id'] != $data['id']) {
                $this->Db->insert('pengunjung', array('kapan' => mktime(), 'news_id' => $data['id'], 'os' => $this->data['broser']['platform'], 'peramban' => $this->data['broser']['parent'], 'ip' => getClientIP(), 'lokasi' => alamatIP()));
                $_SESSION['news_id'] = $data['id'];
            }

            $this->load->view('detail', $this->data);
        }
    }

}

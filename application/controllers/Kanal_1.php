<?PHP

/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Sep 30, 2019 - 4:27:01 PM
 * Filename     : Kanal.php
 * Encoding     : UTF-8
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Kanal extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $id = trim($this->uri->segment(1));
        if ($id == "tag") {
            $tagParam = explode(" ", trim(urldecode($this->uri->segment(2))));

            $this->data['data'] = orCariTag('news', $tagParam, NULL)->result();
        } elseif ($id == "cari") {
            $this->data['data'] = cari('news', array('title' => $this->input->get('q')))->result();
        } else {
            $idSub = trim($this->uri->segment(2));

            if ($idSub) {
                $kanalID = get('kanal', array('slug' => $idSub))->row_array();
                $this->data['data'] = get('news', array('sub_kanal' => $kanalID['id_kanal']))->result();
            } else {
                $kanalID = get('kanal', array('slug' => $id))->row_array();
                $this->data['data'] = get('news', array('kanal_id' => $kanalID['id_kanal']))->result();
            }
        }
        $this->load->view('kanal', $this->data);
    }

}
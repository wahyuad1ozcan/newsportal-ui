<?PHP

/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Sep 30, 2019 - 3:26:43 PM
 * Filename     : Index.php
 * Encoding     : UTF-8
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['isKanal'] = array("kanal_id" => 0);
        $this->load->view('index', $this->data);
    }

}

<?PHP

/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Sep 30, 2019 - 4:27:01 PM
 * Filename     : Kanal.php
 * Encoding     : UTF-8
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Kanal extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->data['isKanal'] = array("kanal_id" => 0);
    }

    public function index() {
        $s1 = trim($this->uri->segment(1));
        $s2 = trim($this->uri->segment(2));

        if ($s1 == "tag") {
            $tagParam = explode(" ", trim(str_replace("-", " ", $s2)));

            $this->data['data'] = orCariTag('news', $tagParam, NULL)->result();
            $this->data['data']['title'] = "Tag berita: " . str_replace("-", " ", $s2);

            $this->load->view('kanal', $this->data);
        } elseif ($s1 == "cari") {
            $this->data['data'] = cari('news', array('title' => $this->input->get('q')))->result();
//            $this->data['data']['title'] = "";
            $this->data['isKanal'] = array("kanal_id" => 0);

            $this->load->view('kanal', $this->data);
        } else {
            if ($s2) {
                $kanalID = get('kanal', array('slug' => $s2))->row_array();
                $this->data['data'] = get('news', array('sub_kanal' => $kanalID['id_kanal'], 'tayang' => 1))->result();

                if (count($kanalID) == 0) {
                    $this->data['data']['title'] = kon('nama_situs') . " &mdash; Halaman tidak diketemukan";
                    $this->load->view('notfound', $this->data);
                } else {
                    if (count($this->data['data']) == 0) {
                        $this->load->view('nonews', $this->data);
                    } else {
                        $this->data['isKanal'] = array("sub_kanal" => $kanalID['id_kanal']);
                        $this->data['data']['title'] = $kanalID['nama_kanal'] . " &mdash; " . $kanalID['meta_desc'];

                        $this->load->view('index', $this->data);
                    }
                }
            } else {
                $kanalID = get('kanal', array('slug' => $s1))->row_array();
                $this->data['data'] = get('news', array('kanal_id' => $kanalID['id_kanal'], 'tayang' => 1))->result();

                if (count($kanalID) == 0) {
                    $this->data['data']['title'] = kon('nama_situs') . " &mdash; Halaman tidak diketemukan";
                    $this->load->view('notfound', $this->data);
                } else {
                    if (count($this->data['data']) == 0) {
                        $this->load->view('nonews', $this->data);
                    } else {
                        $this->data['isKanal'] = array("kanal_id" => $kanalID['id_kanal']);
                        $this->data['data']['title'] = $kanalID['nama_kanal'] . " &mdash; " . $kanalID['meta_desc'];

                        $this->load->view('index', $this->data);
                    }
                }
            }
        }
    }

}

<?PHP

/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Sep 30, 2019 - 3:32:54 PM
 * Filename     : Session.php
 * Encoding     : UTF-8
 */

class Session extends CI_Model {

    function beforeLogin() {
        if ($this->session->userdata('logged_in')) {
            redirect('beranda');
        }
    }

    function afterLogin() {
        if (!$this->session->userdata('logged_in')) {
            redirect("?tipe=3&pesan=Sesi sudah habis, silahkan masuk kembali!");
        }
    }

}

<?PHP

/*
 * @author      : Ahmad Fauzi <info@ahmadfauzi.id>
 * Project Name : eviralo.com
 * Generated    : Sep 30, 2019 - 3:32:04 PM
 * Filename     : Db.php
 * Encoding     : UTF-8
 */

class Db extends CI_Model {

    function config($param) {
        $res = $this->get('config');

        $arr = $res->row_array();
        return $arr[$param];
    }

    function login($email, $pass) {
        $this->db->where(array('username' => $email, 'pass' => md5($pass)));
        $res = $this->db->get('pengguna');

        return $res->row_array();
    }

    function query($query) {
        $res = $this->db->query($query);

        return $res;
    }

    function count($table, $where = NULL) {
        $this->db->from($table);
        if ($where)
            $this->db->where($where);
        return $this->db->count_all_results();
    }

    function get($table, $where = NULL, $limit = NULL, $orderby = NULL) {
        if ($where)
            $this->db->where($where);
        if ($limit)
            $this->db->limit($limit);
        if ($orderby)
            $this->db->order_by($orderby);

        $res = $this->db->get($table);

        return $res;
    }

    function insert($table, $data) {
        $res = $this->db->insert($table, $data);

        return $res;
    }

    function delete($table, $where) {
        $this->db->where($where);
        $res = $this->db->delete($table);

        return $res;
    }

    function update($table, $data, $where) {
        if ($where)
            $this->db->where($where);
        $res = $this->db->update($table, $data);

        return $res;
    }

}
